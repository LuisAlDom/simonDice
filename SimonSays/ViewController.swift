//
//  ViewController.swift
//  SimonSays
//
//  Created by IDS Comercial on 31/01/18.
//  Copyright © 2018 IDS Comercial. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var simonLabel: UILabel!
    @IBOutlet weak var startGameButton: UIButton!
    
    var timer = Timer()
    var simonTimer = Timer()
    
    var timeInt = 20
    var scoreInt = 0
    var gameInt = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        simonLabel.layer.cornerRadius = 10
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(swipeGesture(sender:)))//Se declara la constante swipeRight q llevara direccion right
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight) //Reconocera el gesto y direccion
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(swipeGesture(sender:)))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(swipeGesture(sender:)))
        swipeUp.direction = UISwipeGestureRecognizerDirection.up
        self.view.addGestureRecognizer(swipeUp)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(swipeGesture(sender:)))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        self.view.addGestureRecognizer(swipeDown)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func startGame(_ sender: Any) {
       
        if timeInt == 20{
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
            
            self.updateSimon() //Manda a llamar la funcion
            
            gameInt = 1
            
            startGameButton.isEnabled = false //Se declara asi para cuando el boton sea inabilitado
            startGameButton.alpha = 0.25
        }//if timeInt == 20
        
        if timeInt == 0{//Reinicia las etiquetas
            
            timeInt = 20
            scoreInt = 0
            
            timeLabel.text = String("Time: \(timeInt)")
            scoreLabel.text = String("Score: \(scoreInt)")
            
            startGameButton.setTitle("Start Game", for: UIControlState.normal)
        }//if timeInt == 0
        
    }//StartGame Button
    
     @objc  func updateTimer(){
            timeInt -= 1
            timeLabel.text = String("Time: \(timeInt)")
        
        if timeInt == 0{//Cuando llega a cero detiene los timer
            timer.invalidate()
            simonTimer.invalidate()
            
            simonLabel.text = "Game Over"
            startGameButton.isEnabled = true
            startGameButton.alpha = 1.0
            
            startGameButton.setTitle("Restart", for: UIControlState.normal)
            gameInt = 0
        }
            
    }//UpdateTimer
    
    @objc func updateSimon(){
        let array = ["Simons Says Swipe Right",
                     "Simons Says Swipe Left",
                     "Simons Says Swipe Up",
                     "Simons Says Swipe Down",
                     "Swipe Right",
                     "Swipe Up",
                     "Swipe Down",
                     "Swipe Left"]
        
        let randomWord = Int(arc4random_uniform(UInt32(array.count)))
        simonLabel.text = array[randomWord]
        
        simonTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateSimon), userInfo: nil, repeats: false)
        
    }//UpdateSimon
    
    @objc func swipeGesture(sender: UISwipeGestureRecognizer){ //Va hacer q se vallan sumando
        
        if  gameInt == 1{
            
            if sender.direction == .right{
                simonTimer.invalidate()//Lo detiene para validar
                
                if simonLabel.text == "Simons Says Swipe Right"{
                    scoreInt += 1
                    scoreLabel.text = String("Score: \(scoreInt)")
                    
                    self.updateSimon()//Vuelve a arrancar el timer y cambia a otra palabra
                }else {//simonLabel
                    scoreInt -= 1
                    scoreLabel.text = String("Score: \(scoreInt)")
                    
                    self.updateSimon()
                }//Else
                
            }//sender
            
            if sender.direction == .left{
                simonTimer.invalidate()
                
                if simonLabel.text == "Simons Says Swipe Left"{
                    scoreInt += 1
                    scoreLabel.text = String("Score: \(scoreInt)")
                    
                    self.updateSimon()
                }else {//simonLabel
                    scoreInt -= 1
                    scoreLabel.text = String("Score: \(scoreInt)")
                    
                    self.updateSimon()
                }//Else
                
            }//sender
            
            if sender.direction == .up{
                simonTimer.invalidate()
                
                if simonLabel.text == "Simons Says Swipe Up"{
                    scoreInt += 1
                    scoreLabel.text = String("Score: \(scoreInt)")
                    
                    self.updateSimon()
                }else {//simonLabel
                    scoreInt -= 1
                    scoreLabel.text = String("Score: \(scoreInt)")
                    
                    self.updateSimon()
                }//Else
                
            }//sender
            
            if sender.direction == .down{
                simonTimer.invalidate()
                
                if simonLabel.text == "Simons Says Swipe Down"{
                    scoreInt += 1
                    scoreLabel.text = String("Score: \(scoreInt)")
                    
                    self.updateSimon()
                }else {//simonLabel
                    scoreInt -= 1
                    scoreLabel.text = String("Score: \(scoreInt)")
                    
                    self.updateSimon()
                }//Else
                
            }//sender
            
        }//gameInt
        
    }//SwipeGesture
    
    

}
